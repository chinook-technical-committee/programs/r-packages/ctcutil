% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/util.R
\name{trim_text}
\alias{trim_text}
\title{Trim Text}
\usage{
trim_text(text)
}
\arguments{
\item{text}{A vector of strings to trim whitesapce from both sides}
}
\value{
Text with trailing and leading whitespace removed
}
\description{
Trim whitespace from text.  This function extends the definition
of whitespace beyond the base R function.
}
\examples{
ctcUtil::trim_text(c("   ", " abc ", "xyz   ")) == c("", "abc", "xyz")

}
