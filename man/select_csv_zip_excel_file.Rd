% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/select_files.R
\name{select_csv_zip_excel_file}
\alias{select_csv_zip_excel_file}
\title{Select CSV/Zip/Excel File}
\usage{
select_csv_zip_excel_file(display_caption = "Select Excel or CSV File")
}
\arguments{
\item{display_caption}{What should the title of the file selection dialog say?}
}
\value{
A path and file name
}
\description{
Use the windows GUI to select an Excel or CSV file with either a zip or csv extension
}
