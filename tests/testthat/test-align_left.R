test_that("left align text", {

  test_text <- c("May", "the", "force", "be", "with", "you", "", NA_character_)
  expected_text <- c("May  ",
                     "the  ",
                     "force",
                     "be   ",
                     "with ",
                     "you  ",
                     "     ",
                     "     ")
  left_text <-
    ctcUtil:::align_left(test_text)

  text_length <- nchar(left_text)

  testthat::expect_equal(min(text_length), max(text_length), ignore_attr = TRUE)
  testthat::expect_equal(left_text, expected_text, ignore_attr = TRUE)
})
