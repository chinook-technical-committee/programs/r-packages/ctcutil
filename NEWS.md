# ctcUtil 1.1.2

* Add function to get run time info for other packages

# ctcUtil 1.1.1

 * Add a trim text function to handle non-breakable spaces (nbsp)
 * Fix path issue for connection to MS Access databases
 * Support both ".yaml" and ".yml" in the `select_db_config(...)`

# ctcUtil 1.1.0 

* Support numeric diff when comparing data frames
* Add newline character when writing log messages to console

# ctcUtil 1.0.0

* Fix comparison to handle data frames with spaces and slashes in column names


# ctcUtil 0.1.0

* Initial draft release of the package.
