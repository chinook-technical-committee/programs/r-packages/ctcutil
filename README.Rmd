---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# ctcUtil

<!-- badges: start -->
<!-- badges: end -->

The goal of `ctcUtil` R package is to provide common well tested set of functions that can be used
in other CTC R Packages, such as [REAM](https://gitlab.com/chinook-technical-committee/programs/r-packages/ream) and [CETL](https://gitlab.com/chinook-technical-committee/programs/r-packages/cetl).

## Installation

You can install the latest version of `ctcUtil` from [GitLab](https://gitlab.com/chinook-technical-committee/programs/r-packages/ctcutil) with:

``` r
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/ctcutil.git") 
```

## Example

### Open CAMP Database Connection


```{r open_conn, eval=FALSE, include=TRUE}
library(ctcUtil)

example_config_filename <- "example_config.yaml"
open_camp_connection(example_config_filename)

```

### Compare Data Frames
This is a basic example which shows you how to solve a common problem:

```{r example}
library(ctcUtil)
example_df <- mtcars
example_df$name <- rownames(mtcars)
rownames(example_df) <- NULL
mod_example_df <- example_df

#Remove first to show missing row
mod_example_df <- mod_example_df[2:nrow(mod_example_df),]
#Modify all 6 cylinder cars to 12 cylinders
mod_example_df$cyl <- ifelse(mod_example_df$cyl == 6, 12, mod_example_df$cyl)
diff_df <- compare_dataframe(example_df, mod_example_df, key = "name")
```

```{r diff_df, echo = FALSE,results='asis'}
cat(format_md_table(diff_df, new_line_char = "\r\n"))
```
