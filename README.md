
<!-- README.md is generated from README.Rmd. Please edit that file -->

# ctcUtil

<!-- badges: start -->
<!-- badges: end -->

The goal of `ctcUtil` R package is to provide common well tested set of
functions that can be used in other CTC R Packages, such as
[REAM](https://gitlab.com/chinook-technical-committee/programs/r-packages/ream)
and
[CETL](https://gitlab.com/chinook-technical-committee/programs/r-packages/cetl).

## Installation

You can install the latest version of `ctcUtil` from
[GitLab](https://gitlab.com/chinook-technical-committee/programs/r-packages/ctcutil)
with:

``` r
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/ctcutil.git") 
```

## Example

### Open CAMP Database Connection

``` r
library(ctcUtil)

example_config_filename <- "example_config.yaml"
open_camp_connection(example_config_filename)
```

### Compare Data Frames

This is a basic example which shows you how to solve a common problem:

``` r
library(ctcUtil)
example_df <- mtcars
example_df$name <- rownames(mtcars)
rownames(example_df) <- NULL
mod_example_df <- example_df

#Remove first to show missing row
mod_example_df <- mod_example_df[2:nrow(mod_example_df),]
#Modify all 6 cylinder cars to 12 cylinders
mod_example_df$cyl <- ifelse(mod_example_df$cyl == 6, 12, mod_example_df$cyl)
diff_df <- compare_dataframe(example_df, mod_example_df, key = "name")
```

| name                | mpg | cyl          | disp | hp  | drat | wt   | qsec  | vs  | am  | gear | carb |
|---------------------|-----|--------------|------|-----|------|------|-------|-----|-----|------|------|
| AMC Javelin         |     |              |      |     |      |      |       |     |     |      |      |
| Cadillac Fleetwood  |     |              |      |     |      |      |       |     |     |      |      |
| Camaro Z28          |     |              |      |     |      |      |       |     |     |      |      |
| Chrysler Imperial   |     |              |      |     |      |      |       |     |     |      |      |
| Datsun 710          |     |              |      |     |      |      |       |     |     |      |      |
| Dodge Challenger    |     |              |      |     |      |      |       |     |     |      |      |
| Duster 360          |     |              |      |     |      |      |       |     |     |      |      |
| Ferrari Dino        |     | “6” -\> “12” |      |     |      |      |       |     |     |      |      |
| Fiat 128            |     |              |      |     |      |      |       |     |     |      |      |
| Fiat X1-9           |     |              |      |     |      |      |       |     |     |      |      |
| Ford Pantera L      |     |              |      |     |      |      |       |     |     |      |      |
| Honda Civic         |     |              |      |     |      |      |       |     |     |      |      |
| Hornet 4 Drive      |     | “6” -\> “12” |      |     |      |      |       |     |     |      |      |
| Hornet Sportabout   |     |              |      |     |      |      |       |     |     |      |      |
| Lincoln Continental |     |              |      |     |      |      |       |     |     |      |      |
| Lotus Europa        |     |              |      |     |      |      |       |     |     |      |      |
| Maserati Bora       |     |              |      |     |      |      |       |     |     |      |      |
| (—) Mazda RX4       | 21  | 6            | 160  | 110 | 3.9  | 2.62 | 16.46 | 0   | 1   | 4    | 4    |
| Mazda RX4 Wag       |     | “6” -\> “12” |      |     |      |      |       |     |     |      |      |
| Merc 230            |     |              |      |     |      |      |       |     |     |      |      |
| Merc 240D           |     |              |      |     |      |      |       |     |     |      |      |
| Merc 280            |     | “6” -\> “12” |      |     |      |      |       |     |     |      |      |
| Merc 280C           |     | “6” -\> “12” |      |     |      |      |       |     |     |      |      |
| Merc 450SE          |     |              |      |     |      |      |       |     |     |      |      |
| Merc 450SL          |     |              |      |     |      |      |       |     |     |      |      |
| Merc 450SLC         |     |              |      |     |      |      |       |     |     |      |      |
| Pontiac Firebird    |     |              |      |     |      |      |       |     |     |      |      |
| Porsche 914-2       |     |              |      |     |      |      |       |     |     |      |      |
| Toyota Corolla      |     |              |      |     |      |      |       |     |     |      |      |
| Toyota Corona       |     |              |      |     |      |      |       |     |     |      |      |
| Valiant             |     | “6” -\> “12” |      |     |      |      |       |     |     |      |      |
| Volvo 142E          |     |              |      |     |      |      |       |     |     |      |      |
