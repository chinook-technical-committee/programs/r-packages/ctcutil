
#' Select Database Configuration File
#'
#' Use the windows GUI to select a CAMP configuration or MS Access file name
#'
#' @param display_caption What should the title of the file selection dialog say?
#'
#' @return A path and file name
#'
#' @export
#'
select_db_config <- function(display_caption = "Select CAMP Database Configuration File") {
  config_filter <- rbind(cbind("All files (*.*)", "*.*"),
                         cbind("MS Access database (*.mdb, *.accdb)", "*.mdb;*.accdb"),
                         cbind("Config File (*.config)", "*.config"),
                         cbind("User Config File (*.yaml)", "*.yaml;*.yml"))

  db_filename <- select_file(display_caption, config_filter)
  return(db_filename)
}

#' Select CSV/Zip File
#'
#' Use the windows GUI to select an Excel or CSV file with either a zip or csv extension
#'
#' @param display_caption What should the title of the file selection dialog say?
#'
#' @return A path and file name
#'
#' @export
#'
select_csv_zip_file <- function(display_caption = "Select CSV or Zip File") {
  csv_filter <- rbind(cbind("All files (*.*)", "*.*"),
                      cbind("Zip CSV files (*.zip)", "*.zip"),
                      cbind("CSV files (*.csv)", "*.csv"))
  user_filenames <- select_file(display_caption, csv_filter)
  return(user_filenames)
}

#' Select CSV/Zip/Excel File
#'
#' Use the windows GUI to select an Excel or CSV file with either a zip or csv extension
#'
#' @param display_caption What should the title of the file selection dialog say?
#'
#' @return A path and file name
#'
#' @export
#'
select_csv_zip_excel_file <- function(display_caption = "Select Excel or CSV File") {
  csv_excel_filter <- rbind(cbind("All files (*.*)", "*.*"),
                            cbind("Zip CSV files (*.zip)", "*.zip"),
                            cbind("Excel or CSV files (*.xlsx/*.csv)", "*.xlsx;*.csv"))
  user_filenames <- select_file(display_caption, csv_excel_filter)
  return(user_filenames)
}


#' Select File
#'
#' Select file based provided file extension filter data frame
#'
#' @param display_caption What should the title of the file selection dialog say?
#' @param file_filter Data frame of file filter criteria
#' @param multiple_files Allow users to select multiple files
#'
#' @return A path and file name
#'
select_file <- function(display_caption,
                        file_filter,
                        multiple_files = TRUE) {

  if (exists("choose.files", where = asNamespace("utils"), mode = "function")) {
    user_filenames <- utils::choose.files(default = "",
                                          caption = display_caption,
                                          multi = multiple_files,
                                          filters = file_filter,
                                          index = nrow(file_filter))
  } else {
    stop("Can not use the select_file(...) because the choose.files function is not available in utils package")
  }

  return(user_filenames)
}



